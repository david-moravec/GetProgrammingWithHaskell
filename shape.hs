data Shape = Circle Float | Square Float | Rectangle Float Float

perimeter :: Shape -> Float
perimeter (Circle r) = 2 * 3.1415926 * r
perimeter (Square a) = 4 * a
perimeter (Rectangle a b) = 2 * (a + b)

area :: Shape -> Float
area (Circle r) = 3.1415926 * r^2
area (Square a) = a^2
area (Rectangle a b) = a * b