data FourLetterAlhabet = L1 | L2 | L3 | L4 deriving (Show, Enum, Bounded)

rotN :: (Bounded a, Enum a) => Int -> a -> a
rotN alphabetSize c = toEnum rotation
    where rotation = offset `mod` alphabetSize
          offset = fromEnum c + halfAlphabet
          halfAlphabet = alphabetSize `div` 2

fourLetterAlhabetEncoder :: [FourLetterAlhabet] -> [FourLetterAlhabet]
fourLetterAlhabetEncoder vals = map rot4 vals
    where rot4 = rotN alphaSize
          alphaSize = 1 + fromEnum (maxBound :: FourLetterAlhabet)

rotNdecoder :: (Bounded a, Enum a) => Int -> a -> a
rotNdecoder n c = toEnum encrypted
    where encrypted = rotation `mod` n
          rotation = if even n
                     then fromEnum c + halfN
                     else 1 + fromEnum c + halfN
          halfN = n `div` 2

rotEncoder :: String -> String
rotEncoder text = map rotChar text
    where rotChar = rotN sizeOfAlphabet
          sizeOfAlphabet = 1 + fromEnum(maxBound :: Char)

rotDecoder :: String -> String
rotDecoder text = map rotCharDecoder text
    where rotCharDecoder = rotNdecoder alphabetSize
          alphabetSize = 1 + fromEnum (maxBound :: Char)

xorBool :: Bool -> Bool -> Bool
xorBool b1 b2 = (b1 || b2) && (not (b1 && b2))

xorPair :: (Bool, Bool) -> Bool
xorPair (b1, b2) = xorBool b1 b2

xor :: [Bool] -> [Bool] -> [Bool]
xor list1 list2 = map xorPair (zip list1 list2)

type Bits = [Bool]
intToBits' :: Int -> Bits
intToBits' 0 = [False]
intToBits' 1 = [True]
intToBits' n = if (remainder == 0)
               then False : intToBits' nextVal
               else True : intToBits' nextVal
    where remainder = n `mod` 2
          nextVal = n `div` 2

maxBits :: Int
maxBits = length(intToBits' maxBound)
          
intToBits :: Int -> Bits
intToBits n = leadingFalses ++ reversedBits
    where reversedBits = reverse (intToBits' n)
          leadingFalses = take missingBits (cycle [False])
          missingBits = maxBits - (length reversedBits)

charToBits :: Char -> Bits
charToBits char = intToBits (fromEnum char)

bitsToInt :: Bits -> Int
bitsToInt bits = sum (map (\x -> 2^(snd x)) trueLocations)
    where trueLocations = filter (\x -> fst x == True) (zip bits indeces)
          indeces = [size-1, size-2 .. 0]
          size = length bits

bitsToChar :: Bits -> Char
bitsToChar bits = toEnum (bitsToInt bits)

myPad :: String
myPad = "Shhhhhh"

myPlainText :: String
myPlainText = "Haskell"

applyOTP' :: String -> String -> [Bits]
applyOTP' pad plainText = map (\pair -> (fst pair) `xor` (snd pair))
                                    (zip plainTextBits padBits)
    where plainTextBits = map charToBits plainText
          padBits = map charToBits pad

applyOTP :: String -> String -> String
applyOTP pad plainText = map bitsToChar bitList
    where bitList = applyOTP' pad plainText

encoderDecoder :: String -> String
encoderDecoder = applyOTP myPad 

class Cipher a where
    encode :: a -> String -> String
    decode :: a -> String -> String

data Rot = Rot

instance Cipher Rot where
    encode Rot text = rotEncoder text
    decode Rot text = rotDecoder text

data OneTimePad = OTP String

instance Cipher OneTimePad where
    encode (OTP pad) text = applyOTP pad text
    decode (OTP pad) text = applyOTP pad text

myOTP :: OneTimePad
myOTP = OTP (cycle[minBound .. maxBound])

prng :: Int -> Int -> Int -> Int -> Int
prng a b maxNumber seed = (a*seed + b) `mod` maxNumber

prngChar :: Int -> Char
prngChar seed = toEnum prngChar
    where prngChar = prng 1337 7 (fromEnum (maxBound :: Char)) seed

generateRandomString :: Int -> String
generateRandomString seed = nextVal : generateRandomString (fromEnum nextVal)
    where nextVal = prngChar seed

data StreamCipher = SC Int

instance Cipher StreamCipher where
    encode (SC seed) text = applyOTP (generateRandomString seed) text
    decode (SC seed) text = applyOTP (generateRandomString seed) text

stream5 :: StreamCipher
stream5 = SC 5